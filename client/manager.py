from jsonrpclib.SimpleJSONRPCServer import SimpleJSONRPCServer
from jsonrpclib import Server
import time
import json

class Console:
	"""
	register_slave(string hostname, int port)
		rejestruje nowego pracownika

	register_task(string task_data)
		zwraca id zadania

	list_slaves():
		zwraca liste slownikow, gdzie kazdy slownik ma postac:
		{"id": 1, "hostname": "localhost", "port": 1234}

	list_tasks():
		zwraca liste slownikow, gdzie kazdy slownik ma postac:
		{"id": 1, "data": "<root>abc</root>"}

	refresh():

	assign_task(int task_id, int slave_id):
		ustawia zadanie dla pracownika
	

	"""

	def __init__(self, settings):
		self.hostname = settings["hostname"]
		self.port     = settings["port"]
		self.server   = Server (
			"http://%s:%s" % (self.hostname, self.port)
		)
		self.test()

	def print_slaves(self):
		for slave in self.server.list_slaves():
			print (
				"% 3d %s %d %s" % (
					slave["id"], slave["hostname"], slave["port"], slave["status"]
				)
			)

	def test(self):
		self.print_slaves()
		self.server.register_task("<abc>123</abc>")
		self.server.register_task("<def>123</def>")
		for task in self.server.list_tasks():
			print( "% 3d %s" % (task["id"], task["data"]) )

		self.server.assign_task(0, 0)
		self.server.assign_task(1, 1)

		self.server.prepare_all()

		while True:
			time.sleep(1)
			self.print_slaves()
			_ss = [s for s in self.server.list_slaves() if s["status"] != "prepared"]
			if len(_ss) == 0: break

		self.server.run_all()

		while True:
			time.sleep(1)
			self.print_slaves()
			_ss = [
				s for s in self.server.list_slaves() if s["status"] != "completed"
			]
			if len(_ss) == 0: break

if __name__ == "__main__":
	with open("settings.json") as f:
		settings = json.loads( f.read() )

	m = Console(settings)
