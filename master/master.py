# -*- coding: utf-8 -*-
from jsonrpclib.SimpleJSONRPCServer import SimpleJSONRPCServer
from jsonrpclib import Server
import sys, traceback
import json

class Status:
	Waiting   = "waiting"
	HasTask   = "has_task"
	Prepared  = "prepared"
	Running   = "running"
	Completed = "completed"

class Slave(Server):
	
	def __init__(self, hostname, port, id):
		self.id       = id
		self.hostname = hostname
		self.port     = port
		self.status   = Status.Waiting
		Server.__init__( self, "http://%s:%d" % (hostname, port) )

	def serialize(self):
		return {
			"id"      : self.id,
			"hostname": self.hostname,
			"port"    : self.port,
			"status"  : self.status
		}

class Master(SimpleJSONRPCServer):

	def __init__(self, settings):
		self.slaves = [ ]
		self.tasks  = [ ]
		self.hostname = settings["hostname"]
		self.port     = settings["port"]

		SimpleJSONRPCServer.__init__( self, (self.hostname, self.port) )
		self.register_function(self.register_slave)
		self.register_function(self.register_task)
		self.register_function(self.list_slaves)
		self.register_function(self.list_tasks)
		self.register_function(self.assign_task)
		self.register_function(self.assign_and_prepare_task)
		self.register_function(self.refresh)
		self.register_function(self.prepare_all)
		self.register_function(self.run_all)
		self.register_function(self.report_task_prepared)
		self.register_function(self.report_task_completed)

		self.serve_forever()

	def _get_slave(self, hostname, port):
		for slave in self.slaves:
			if slave.hostname == host and slave.port == port:
				return slave

		raise ValueError("slave with specified hostname and port does not exit")

	def register_slave(self, hostname, port):
		"""
		hostname: string
		port: int

		rejestruje nowego slave, zwraca id
		"""
		self.slaves.append( Slave( hostname, port, len(self.slaves) ) )
		return len(self.slaves) - 1

	def register_task(self, task_data):
		"""
		task_data: string

		rejestruje nowe zadanie, zwraca id
		"""
		self.tasks.append(task_data)
		return len(self.tasks) - 1

	def list_slaves(self):
		"""
		zwraca liste slownikow, gdzie kazdy slownik ma postac:
		{"id": 1, "hostname": "localhost", "port": 1234, "status":"waiting"}

		dostępne statusy:
		- waiting
		- has_task
		- prepared
		- running
		- completed
		"""
		return [s.serialize() for s in self.slaves]

	def list_tasks(self):
		"""
		zwraca liste slownikow, gdzie kazdy slownik ma postac:
		{"id": 1, "data": "<root>abc</root>"}
		"""
		return [{"id": int(i[0]), "data": i[1]} for i in enumerate(self.tasks)]

	def assign_task(self, slave_id, task_id):
		"""
		task_id: int
		slave_id: int

		ustawia zadanie dla pracownika
		"""
		slave = self.slaves[slave_id]
		task  = self.tasks[task_id]
		slave.assign_task(task)
		slave.status = Status.HasTask

	def prepare_all(self):
		"""
		przygotowuje wszystkich slave, którzy otrzymali zadanie
		"""
		slaves = [s for s in self.slaves if s.status == Status.HasTask]
		[s.prepare() for s in slaves]

	def run_all(self):
		"""
		uruchamia wszystkich slaves, którzy przygotowali zadanie
		"""
		slaves = [s for s in self.slaves if s.status == Status.Prepared]
		for s in slaves:
			s.run()
			s.status = Status.Running

	def report_task_prepared(self, slave_id):
		self.slaves[slave_id].status = Status.Prepared

	def report_task_completed(self, slave_id):
		self.slaves[slave_id].status = Status.Completed

	def assign_and_prepare_task(self, slave_id, task_id):
		self.assign_task(slave_id, task_id)
		self.slaves[slave_id].prepare()

	def refresh(self):
		for slave in self.slaves:
			slave.refresh()

if __name__ == "__main__":
	with open("settings.json") as f:
		settings = json.loads( f.read() )

	m = Master(settings)

