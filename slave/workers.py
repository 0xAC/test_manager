# workery
import time

#status
OK = 0

#base example result
BASE_RESULT = """
<testResult>
    <status>OK</status>
    <time>15s</time>
</testResult>"""


class BaseWorker():
	def __init__(self, slave):
		self.slave = slave

	def prepare(self, task):
		self.l=len(task)
		if self.l>20:self.l=20
		time.sleep(10)

	def run(self):
		for i in range(5):
			time.sleep(8)
			self.slave.append("<part_res>Partial result </part_res>")
		time.sleep(1)
		return BASE_RESULT

WORKDICT={
	"base":BaseWorker
}

