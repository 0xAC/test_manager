from jsonrpclib.SimpleJSONRPCServer import SimpleJSONRPCServer
from jsonrpclib import Server
import json
import threading
import workers
import sys
import time
from Queue import Queue

def worker_thread(slave, data, queue):
	w = workers.WORKDICT["base"](slave)
	w.prepare(data)
	slave._set_status("prepared")
	queue.get()
	w.run()
	slave._set_status("completed")

def run(master_settings, slave_settings):
	s = Slave(master_settings, slave_settings)

class Master(Server):

	def __init__(self, hostname, port):
		Server.__init__( self, "http://%s:%s" % (hostname, port) )

class Slave(SimpleJSONRPCServer):

	def __init__(self, master_settings, slave_settings):
		self.master = Master (
			master_settings["hostname"],
			master_settings["port"]
		)

		self.hostname = slave_settings["hostname"]
		self.port     = slave_settings["port"]
		self.status   = None
		self.queue    = None
		self.refresh()

		self.id = self.master.register_slave(self.hostname, self.port)

		SimpleJSONRPCServer.__init__ (
			self, (self.hostname, self.port)
		)

		t = threading.Thread(target = self._monitor)
		t.setDaemon(True)
		t.start()

		self.register_function(self.assign_task)
		self.register_function(self.prepare)
		self.register_function(self.refresh)
		self.register_function(self.run)
		self.register_function(self.shutdown)
		self.serve_forever()

	def _monitor(self):
		while True:
			if self.status is not None:
				if self.status == "prepared":
					self.master.report_task_prepared(self.id)
				else:
					self.master.report_task_completed(self.id)
				self.status = None

			time.sleep(2)

	def _set_status(self, status):
		self.status = status

	def append(self, data):
		self.partials.append(data)

	def assign_task(self, data):
		self.task = data

	def refresh(self):
		self.worker   = None
		self.partials = [  ]
		self.task     = None

	def prepare(self):
		self.queue = Queue()
		t = threading.Thread (
			target = worker_thread, args = (self, self.task, self.queue)
		)
		t.start()

	def run(self):
		assert(self.queue is not None)
		self.queue.put(1)

if __name__ == "__main__":
	config = "settings.json"

	if len(sys.argv) > 1:
		config = sys.argv[1]

	with open(config) as f:
		settings = json.loads( f.read() )

	ts = [ ]
	for slave in settings["slaves"]:
		t = threading.Thread (
			target = run,
			args = (settings["master"], slave)
		)

		t.setDaemon(True)
		ts.append(t)
		t.start()

	try:
		while len(ts) > 0:
			[t.join(1) for t in ts]
			ts = [t for t in ts if t.is_alive()]
	except KeyboardInterrupt:
		sys.exit(0)
