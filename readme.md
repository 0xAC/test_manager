# Architektura

## Server

### Zarządzanie:

Technologia: *JSON-RPC*

### Komunikacja z klientami:

Technologia: JSON-RPC

## Client

### Komunikacja z serwerem

Technologia: *JSON-RPC*

### Statusy

- oczekuje na zadanie
- przygotowanie
- gotowy
- realizacja

# TODO
- odświeżanie stanu
- przechowywanie stanu w bazie danych
- przechwytywanie wyników cząstkowych
